import { Component } from '@angular/core';
import { DirectionsMapDirective } from 'map/google-map.directive';
import { Http, Response } from '@angular/http';
import { MarkersService } from './markersService';


import { Marker } from 'models/Marker';
import 'rxjs/add/operator/toPromise';

@Component({
selector: 'app-root',
providers: [MarkersService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


  
export class AppComponent {
  title: string = 'Poi';
  lat: number = -6.1753924;
  lng: number = 106.8255827;
  zoom: number = 15;
  initial: boolean = true;
  
  markers = [];
  labels:string[] = []; 
  
  result: Object;
  
  init: boolean = true;
  origin = {};
  destination = {};
  
  constructor(private markersService: MarkersService) {
    markersService.getMarkers()
      .subscribe(
        markers => { this.markers = markers, this.mapMarkers(this.markers) },
        () => console.log('Completed!')
      );
  }
  
  public mapClicked($event: any) {
    if(!this.initial){
        this.markers.pop();
    }
    this.markers.push({
      id: "",
      label: "",
      lat: $event.coords.lat,
      lng: $event.coords.lng,
      desc: ""
    });
    this.initial = false;
  }
  
  public addPOI(newlabel : string, newdesc : string) {
    this.markers[this.markers.length - 1].label = newlabel;
    this.markers[this.markers.length - 1].desc = newdesc;
    this.markersService.createMarker(this.markers[this.markers.length - 1])
      .subscribe(
        markers => { this.markers.pop(), this.markers.push(markers), this.mapMarkers(this.markers)},
        () => console.log('Completed!')
      );
    this.initial = true;
  }
  
  public deletePOI(id : number, index : number) {
    this.markersService.deleteMarker(id)
      .subscribe(markers => { this.markers.splice(index,1), this.mapMarkers(this.markers)});
    console.log(id+"-"+index);
  }
  
  public mapMarkers(newMarkers){
    this.labels = newMarkers.map(this.createLabel);
  }
  
  public createLabel(marker, index: number){
    var uniqueLabel = [marker.id, marker.label].join(" - ");
    return uniqueLabel;
  }
  
  public getRoute( fromLabel : any, destLabel : any ){
  this.init=false;
   for (let marker of this.markers){
    if(marker.id == fromLabel.split(" - ")[0]){
        var fromMarkerLat = marker.lat;
        var fromMarkerLng = marker.lng;
    }else if(marker.id == destLabel.split(" - ")[0]){ 
        var destMarkerLat = marker.lat;
        var destMarkerLng = marker.lng;
    }
   }
   this.origin = { longitude: fromMarkerLng, latitude: fromMarkerLat };
   this.destination = { longitude: destMarkerLng, latitude: destMarkerLat };

  }
  
  public removeRoute(){
    this.origin = {};
    this.destination = {};
  };
}
