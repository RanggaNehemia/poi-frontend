import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class MarkersService {
  constructor(private http: Http) { }
  
  private markerUrl = 'https://sheltered-spire-93873.herokuapp.com/get-markers';
  private createUrl = 'https://sheltered-spire-93873.herokuapp.com/add-marker';
  private deleteUrl = 'https://sheltered-spire-93873.herokuapp.com/delete-marker/';
  
  getMarkers() {
    return this.http.get(this.markerUrl)
      .map(response => response.json());
  }
  
  createMarker( marker ) {
    return this.http.post(this.createUrl, { "label" : marker.label, "desc" : marker.desc, "lat" : marker.lat, "lng" : marker.lng })
      .map(response => response.json());
  }
  
  deleteMarker( id ) {
    return this.http.delete(this.deleteUrl+id)
      .map(response => response.json());
  }
}
