export class Marker {
  id : number;
  label : string;
  desc : string;
  lat : number;
  lng : number;

  constructor(markerInfo:any) {
    this.id = markerInfo.id;
    this.label = markerInfo.label;
    this.desc = markerInfo.desc;
    this.lat = markerInfo.lat;
    this.lng = markerInfo.lng;
  }

  save() {
    // HTTP request would go here
    console.log(this.id, this.label, this.desc, this.lat, this.lng);
  }
}